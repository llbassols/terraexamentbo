package cat.dam.lluc.holanom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//importem classes necessàries
import android.widget.TextView;
public class Main2Activity extends AppCompatActivity {
    //declarem un TextView que apuntarà al de la interfície
    private TextView tv_salutacio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        //Localitzar els controls
        tv_salutacio = (TextView)findViewById(R.id.activity_salutacio_tv_salutacio);
        //Recuperem la informació passada per l'intent
        Bundle bundle = this.getIntent().getExtras();
        //Construïm el missatge a mostrar
        tv_salutacio.setText("Hola " + bundle.getString("NOM"));
    }
}